# GitLab Trigger Validator

This project produces a CLI tool that can be used to provide further validation around triggered pipelines and their additional variables.
After execution, it produces a `env` file that can be sourced in downstream stages to perform any additional tasks.

## Usage

The tool is currently not published to NPM or any other package manager so you can either install it yourself or use it from the
container image that's automatically built from this repo's pipeline.

```cli
docker run code.vt.edu:5005/mikesir/gitlab-trigger-validator gitlab-trigger-validator <options>
```

The following options are available for the CLI. The `--configFile` and `--token` are required.


```
Usage: gitlab-trigger-validator [options]

This project provides the ability to validate pipeline triggers and the additional 
properties in the trigger.

Options:
  -v, --version                    output the version number
  --gitLabHost <host>              GitLab hostname (default: $CI_SERVER_HOST)
  -c, --configFile <fileLocation>  location of config file
  -t, --token <token>              JWT token passed as part of the pipeline trigger
  -o, --outputFile <outputFile>    Location to save env export (default: "trigger.env")
  -h, --help                       display help for command
```

## Configuration

In order to validate requests, a config file is needed to provide details on what variables requests are allowed to make.
A couple of notes about the configuration file:

- While the usage of `_all` is supported at both the project root and each ref type, it is not required.
- The priority or rules starts from the project root and works its way inward (i.e. branch-specific settings will override any `_all` settings)
- Both branch/tag names and env variable values support either exact string matches or regular expressions. Regular expressions _must_ begin and end with a `/`.

### Sample Config

```yaml
triggerSources:
  repo-path:

    # Settings that apply to all branches and tags
    _all:
      properties:
        ENV_NAME: ENV_VALUE
        LOCATION: /aws.*/

    # Settings for only branches
    branches:

      # Settings that apply to all branches
      _all:
        properties:
          ENV_NAME: ANOTHER_VALUE

      # Settings that apply only to the master branch
      master:
        properties:
          ENV_NAME: master

      # Settings that apply only to branches that match the /test-.*/ regular expression
      /test-.*/:
        properties:
          ENV_NAME: test

    # Settings for only tags
    tags:

      # Settings that apply to all tags
      _all:
        properties:
          ENV_NAME: ANOTHER_VALUE

      # Settings that apply only to the staging tag
      staging:
        properties:
          ENV_NAME: staging

      # Settings that apply only to tags that match the /v.*/ regular expression
      /v.*/:
        properties:
          ENV_NAME: prod
```



## Why's this project needed?

Imagine a scenario in which you have two or more repos that fit the following needs:

- **Application repos** - application code that produces various artifacts (such as container images)
- **Deployment repo** - contains deployment manifests (Kubernetes manifests, Terraform, etc.)

To support the ability for an application repo to deploy updated versions of code, the application repo can include a stage in the pipeline
that triggers a pipeline on the deployment repo and pass along variables to configure the deploy (what app, what repo, etc.). It might look like this...

```mermaid
graph LR;
    subgraph appRepo [App Repo]
    build[Build Artifact]-->trigger[Trigger Pipeline]
    end
    subgraph deployRepo [Deploy Repo]
    trigger-->deploy[Do deploy]
    end
```

But, what if we have multiple app repos pointing to the same deploy repo?

```mermaid
graph LR;
    app1[App 1]-->deploy[Deploy Repo]
    app2[App 2]-->deploy[Deploy Repo]
    app3[App 3]-->deploy[Deploy Repo]
```

We will quickly want to have the ability to ensure **App 1 can only make requests to deploy App 1**. In addition, we might have further desires
to do things like **only the _master_ branch can deploy to staging and only tags can go to prod**. How would we do that? We need a higher form of
validation.

## Validating Requests

To validate requests, we need to know, with certainty, who the caller is. With GitLab, pipelines are triggered using only a token. This _could_ be
shared across projects (please don't do that though). We also want something a little stronger than simple variables passed along with the trigger request.

Starting with GitLab 12.10, every job has a `CI_JOB_JWT` that is a JWT that includes claims about the current job and is signed by GitLab's own keys.
If we were to pass this along with the trigger request, the triggered pipeline can validate the token and know more details about the responsible job. Some of
those claims include the following:

```json
{
  "namespace_id": "2",
  "namespace_path": "my-group",
  "project_id": "1234",
  "project_path": "my-group/my-repo",
  "user_id": "10",
  "user_login": "pinky",
  "user_email": "pinky@acme.com",
  "pipeline_id": "12345",
  "job_id": "23456",
  "ref": "master",
  "ref_type": "branch",
  "ref_protected": "false",
  "jti": "a54521d9-6dfc-4cd2-8b43-4c401dbb36cc",
  "iss": "my-gitlab-instance.com",
  "iat": 1591112444,
  "nbf": 1591112439,
  "exp": 1591116044,
  "sub": "job_23456"
}
```

Now, the pipeline would look something like this...


```mermaid
graph LR;
    subgraph appRepo [App Repo]
    build[Build Artifact]-->trigger[Trigger Pipeline]
    end
    subgraph deployRepo [Deploy Repo]
    trigger-->validate[Validate JWT and request]
    validate-->deploy[Do deploy]
    end
```

