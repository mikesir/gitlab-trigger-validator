FROM node:lts-alpine AS base
WORKDIR /app
COPY package.json yarn.lock ./

FROM base AS test
RUN yarn install && yarn cache clean
COPY bin ./bin
COPY spec ./spec
COPY src ./src
RUN yarn test

FROM base AS final
COPY --from=test /app/bin ./bin
COPY --from=test /app/src ./src
RUN yarn global add file:$PWD --production && \
    yarn cache clean && \
    gitlab-trigger-validator --version
