const jwt = require("jsonwebtoken");
const jwks = require("jwks-rsa");

async function verifyToken(gitlabHostPath, token) {
  const jwksClient = jwks({
    jwksUri: `${gitlabHostPath}/-/jwks`,
  });

  function getKey(header, cb) {
    jwksClient.getSigningKey(header.kid, (err, key) => {
      if (err) return cb(err);
      cb(null, key.publicKey || key.rsaPublicKey);
    });
  }

  return new Promise((acc, rej) => {
    jwt.verify(token, getKey, (err, decoded) => {
      if (err) return rej(err);
      acc(decoded);
    });
  });
}

module.exports = verifyToken;
