const fs = require("fs");

function saveOutput(fileLocation, config, variables) {
  const output = Object.keys(config)
    .map((k) => `${k}=${variables[k]}`)
    .join("\n");

  fs.writeFileSync(fileLocation, output);
  console.log(`- Output saved to ${fileLocation}`);
}

module.exports = saveOutput;
