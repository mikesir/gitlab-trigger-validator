const verifyToken = require("../src/tokenVerifier");
const configLoader = require("../src/configLoader");
const validateVariablesAgainstConfig = require("../src/validator");
const saveOutput = require("../src/outputSaver");

async function run(programOptions, incomingVariables) {
  const { gitLabHost, token, configFile, outputFile } = programOptions;
  const callerDetails = await verifyToken(`https://${gitLabHost}`, token);
  const applicableConfig = configLoader(configFile, callerDetails);

  validateVariablesAgainstConfig(applicableConfig, incomingVariables);

  saveOutput(outputFile, applicableConfig, incomingVariables);
}

module.exports = run;
