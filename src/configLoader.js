const fs = require("fs");
const yaml = require("js-yaml");

function extractApplicableConfig(configLocation, callerDetails) {
  console.log(`Loading config from ${configLocation}`);

  if (!fs.existsSync(configLocation)) {
    throw new Error(`Unable to find file '${configLocation}'`);
  }

  const config = yaml.safeLoad(fs.readFileSync(configLocation, "utf8"));
  const projectInfo = config.triggerSources[callerDetails.project_path];

  if (projectInfo === undefined) {
    throw new Error(`Found no configuration for '${callerDetails.project_path}'`);
  }

  console.log(`- Using configuration from ${callerDetails.project_path}`);

  const validationConfig = {};

  // Grab the default _all, if set
  if (projectInfo._all) {
    console.log("  - Applying config from '_all'");
    Object.assign(validationConfig, projectInfo._all.properties);
  }

  const refType = callerDetails.ref_type;
  const refTypeConfigName = refType === "branch" ? "branches" : "tags";
  const refTypeSettings = projectInfo[refTypeConfigName];

  if (refTypeSettings) {
    // Pull in branches._all or tags._all, if set
    if (refTypeSettings._all) {
      console.log(`  - Applying config from '${refTypeConfigName}._all'`);
      Object.assign(validationConfig, refTypeSettings._all.properties);
    }

    // If there's a specific name for this ref, use it
    if (refTypeSettings[callerDetails.ref]) {
      console.log(`  - Applying config from '${refTypeConfigName}.${callerDetails.ref}'`);
      Object.assign(validationConfig, refTypeSettings[callerDetails.ref].properties);
    }

    // Check for any regex matches
    else {
      Object.keys(refTypeSettings)
        .filter((n) => n.startsWith("/") && n.endsWith("/"))
        .filter((n) => callerDetails.ref.match(new RegExp(n.substr(1, n.length - 2))))
        .forEach((n) => {
          console.log(`  - Applying config from '${refTypeConfigName}.${n}'`);
          Object.assign(validationConfig, refTypeSettings[n].properties);
        });
    }
  }

  return validationConfig;
}

module.exports = extractApplicableConfig;
