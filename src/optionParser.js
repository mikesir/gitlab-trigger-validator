const { Command } = require("commander");
const fs = require("fs");

function parseOptions(args) {
  const program = new Command();
  program.version("0.1.0", "-v, --version");

  program.description(
    "This project provides the ability to validate pipeline triggers and the additional \n" +
      "properties in the trigger."
  );

  program
    .exitOverride()
    .requiredOption(
      "--gitLabHost <host>",
      "GitLab hostname (default: $CI_SERVER_HOST)",
      process.env.CI_SERVER_HOST
    )
    .requiredOption("-c, --configFile <fileLocation>", "location of config file")
    .requiredOption("-t, --token <token>", "JWT token passed as part of the pipeline trigger")
    .requiredOption("-o, --outputFile <outputFile>", "Location to save env export", "trigger.env");

  program.parse(args);

  const options = program.opts();

  if (!fs.existsSync(options.configFile))
    throw new Error(`error: unable to find configFile named ${options.configFile}`);

  return options;
}

module.exports = parseOptions;
