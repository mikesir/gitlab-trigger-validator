function validator(applicableConfig, variables) {
  // Filter out the valid keys and keep only the invalid ones
  const invalidKeys = Object.keys(applicableConfig).filter((key) => {
    if (variables[key] === undefined) return true;

    const expectedValue = applicableConfig[key];

    if (expectedValue.startsWith("/") && expectedValue.endsWith("/")) {
      const regEx = new RegExp(expectedValue.substr(1, expectedValue.length - 2));
      return variables[key].match(regEx) === null;
    }

    return variables[key] !== expectedValue;
  });

  if (invalidKeys.length === 0) {
    console.log("- All configuration validation passed");
    return true;
  }

  const errorDetails = invalidKeys.map((key) => ({
    key,
    expected: applicableConfig[key],
    actual: variables[key],
  }));

  const errorDetailString = errorDetails
    .map((k) => `- ${k.key}: \n  - Needs to match: ${k.expected}\n  - Actual: ${k.actual}`)
    .join("\n");

  const plurality = invalidKeys.length === 1 ? "property was" : "properties were";

  const error = new Error(
    `Validation failed. The following ${plurality} in error:\n${errorDetailString}`
  );
  error.details = errorDetails;
  throw error;
}

module.exports = validator;
