const validator = require("../src/validator");

const APP_CONFIG = {
  ENV: "test",
  IMAGE: "/some/image:.*/",
};

it("throws validation error when expected property not defined", () => {
  try {
    validator(APP_CONFIG, {});
    fail("should have thrown");
  } catch (err) {
    expect(err.details).toBeDefined();
    expect(err.details.length).toBe(2);
    expect(err.details[0].actual).toBe(undefined);
  }
});

it("throws when the value doesn't exactly match string", () => {
  try {
    validator(APP_CONFIG, { ENV: "teest", IMAGE: "some/image:test" });
    fail("should have thrown");
  } catch (err) {
    expect(err.details).toBeDefined();
    expect(err.details.length).toBe(1);
    expect(err.details[0].key).toBe("ENV");
  }
});

it("throws when the value doesn't match regex pattern", () => {
  try {
    validator(APP_CONFIG, { ENV: "test", IMAGE: "some/imagge:test" });
    fail("should have thrown");
  } catch (err) {
    expect(err.details).toBeDefined();
    expect(err.details.length).toBe(1);
    expect(err.details[0].key).toBe("IMAGE");
  }
});

it("does not throw when everything is valid", () => {
  validator(APP_CONFIG, { ENV: "test", IMAGE: "some/image:test" });
});
