const fs = require("fs");
const tokenVerifier = require("../src/tokenVerifier");
const ServerMock = require("mock-http-server");
const jwks = require("./keys/jwks.json");
const jwt = require("jsonwebtoken");
const privateKey = fs.readFileSync(__dirname + "/keys/privateKey.pem");

// Needed to get around https://github.com/axios/axios/issues/1754
// The jwks-rsa library uses axios
const axios = require("axios");
axios.defaults.adapter = require("axios/lib/adapters/http");

let mockServer, token;

beforeAll((cb) => {
  mockServer = new ServerMock({ host: "localhost" });

  mockServer.on({
    method: "GET",
    path: "/-/jwks",
    reply: {
      headers: { "content-type": "application/json" },
      body: (req, res) => res(JSON.stringify(jwks)),
    },
  });

  mockServer.start(cb);
});

afterAll((cb) => {
  mockServer.stop(cb);
});

beforeEach(() => {
  token = jwt.sign(
    {
      ref: "master",
      ref_type: "branch",
    },
    privateKey,
    { algorithm: "RS256", keyid: "test-key" }
  );
});

afterEach(() => {
  mockServer.resetRequests();
});

it("passes when everything is setup correctly", async () => {
  await tokenVerifier("http://localhost:" + mockServer.getHttpPort(), token);
  expect(mockServer.requests({ path: "/-/jwks" }).length).toBe(1);
});

it("fails when the token uses an unknown key", async () => {
  token = jwt.sign(
    {
      ref: "master",
      ref_type: "branch",
    },
    privateKey,
    { algorithm: "RS256", keyid: "unknown-key-id" }
  );

  try {
    await tokenVerifier("http://localhost:" + mockServer.getHttpPort(), token);
    fail("should have thrown");
  } catch (err) {
    expect(err.message).toContain("Unable to find a signing key");
  }
});
