const fs = require("fs");
const cliParser = require("../src/optionParser");

const CONFIG_FILE_NAME = "config.yml";

const execArgs = ["node", "./bin/validator"];

const gitLabOptions = ["--gitLabHost", "gitlab.com"];

const configFileOptions = ["-c", CONFIG_FILE_NAME];

const tokenOptions = ["-t", "some-token"];

const goodOptions = [...execArgs, ...gitLabOptions, ...configFileOptions, ...tokenOptions];

beforeEach(() => {
  process.env = {};
  delete process.env.CI_SERVER_HOST;
  fs.writeFileSync(CONFIG_FILE_NAME, "");
});

afterEach(() => {
  fs.unlinkSync(CONFIG_FILE_NAME);
});

it("works when everything is defined", () => {
  const options = cliParser(goodOptions);

  expect(options.gitLabHost).toBe("gitlab.com");
  expect(options.configFile).toBe("config.yml");
  expect(options.token).toBe("some-token");
});

it("throws when no gitLabHost defined", () => {
  try {
    cliParser([...execArgs, ...configFileOptions, ...tokenOptions]);
    fail("Should have thrown");
  } catch (err) {
    expect(err.message).toContain("gitLabHost");
  }
});

it("uses env.CI_SERVER_HOST when defined", () => {
  process.env.CI_SERVER_HOST = "gitlab.com";
  const options = cliParser([...execArgs, ...configFileOptions, ...tokenOptions]);

  expect(options.gitLabHost).toBe("gitlab.com");
});

it("requires a config file to be specified", () => {
  try {
    cliParser([...execArgs, ...gitLabOptions, ...tokenOptions]);
    fail("Should have thrown");
  } catch (err) {
    expect(err.message).toContain("configFile");
  }
});

it("requires a JSON token to be specified", () => {
  try {
    cliParser([...execArgs, ...gitLabOptions, ...configFileOptions]);
    fail("Should have thrown");
  } catch (err) {
    expect(err.message).toContain("token");
  }
});

it("fails if the config file is not found", () => {
  try {
    cliParser([...execArgs, ...gitLabOptions, ...tokenOptions, "-c", "not-found.yml"]);
  } catch (err) {
    expect(err.message).toContain("unable to find configFile");
  }
});
