const fs = require("fs");
const loadConfig = require("../src/configLoader");

const CALLER_DETAILS = {
  project_path: "group/repo",
  ref: "master",
  ref_type: "branch",
};

it("fails when config file not found", () => {
  try {
    loadConfig("unknown.yml", { project_path: "unknown" });
    fail("should have thrown");
  } catch (err) {
    expect(err.message).toContain("Unable to find file 'unknown.yml'");
  }
});

it("fails when project not configured as a source", () => {
  try {
    loadConfig(__dirname + "/config/simple.yml", { project_path: "unknown" });
    fail("should have thrown");
  } catch (err) {
    expect(err.message).toContain("Found no configuration for 'unknown'");
  }
});

it("loads a simple config using exact match on the branch ref", () => {
  const config = loadConfig(__dirname + "/config/simple.yml", CALLER_DETAILS);

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(1);
  expect(config.ENV).toBe("staging");
});

it("loads a simple config using regex match on the branch ref", () => {
  const config = loadConfig(__dirname + "/config/simple.yml", { ...CALLER_DETAILS, ref: "test" });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(1);
  expect(config.ENV).toBe("qa");
});

it("loads a simple config using exact match on the tag ref", () => {
  const config = loadConfig(__dirname + "/config/simple.yml", {
    ...CALLER_DETAILS,
    ref: "beta",
    ref_type: "tag",
  });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(1);
  expect(config.ENV).toBe("beta");
});

it("loads a simple config using regex match on the tag ref", () => {
  const config = loadConfig(__dirname + "/config/simple.yml", {
    ...CALLER_DETAILS,
    ref: "v1.0.0",
    ref_type: "tag",
  });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(1);
  expect(config.ENV).toBe("prod");
});

it("includes _all when loading config", () => {
  const config = loadConfig(__dirname + "/config/complex.yml", CALLER_DETAILS);

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(2);
  expect(config.ENV).toBe("staging");
  expect(config.LOCATION).toBe("aws");
});

it("allows override of _all properties in more specific config", () => {
  const config = loadConfig(__dirname + "/config/complex.yml", { ...CALLER_DETAILS, ref: "test" });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(2);
  expect(config.ENV).toBe("qa");
  expect(config.LOCATION).toBe("qa");
});

it("allows _all to be defined at the refType level and overrides root", () => {
  const config = loadConfig(__dirname + "/config/complex.yml", {
    ...CALLER_DETAILS,
    ref: "v1.0.0",
    ref_type: "tag",
  });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(2);
  expect(config.ENV).toBe("prod");
  expect(config.LOCATION).toBe("aws-secure");
});

it("allows _all to be defined at the ref level and overrides both root and refType", () => {
  const config = loadConfig(__dirname + "/config/complex.yml", {
    ...CALLER_DETAILS,
    ref: "beta",
    ref_type: "tag",
  });

  expect(config).not.toBeUndefined();
  expect(Object.keys(config).length).toBe(2);
  expect(config.ENV).toBe("beta");
  expect(config.LOCATION).toBe("aws-beta");
});
