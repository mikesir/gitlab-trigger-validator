const fs = require("fs");
const saveOutput = require("../src/outputSaver");

const FILE_LOCATION = "test.env";

const CONFIG = {
  ENV: "prod",
  APP: "/test/",
};

const ENV_VARIABLES = {
  ENV: "prod",
  APP: "test",
};
afterEach(() => {
  if (fs.existsSync(FILE_LOCATION)) fs.unlinkSync(FILE_LOCATION);
});

it("stores all properties correctly", () => {
  saveOutput(FILE_LOCATION, CONFIG, ENV_VARIABLES);

  const output = fs.readFileSync(FILE_LOCATION).toString();
  expect(output).toBe("ENV=prod\nAPP=test");
});

it("ignores env variables not defined in the config", () => {
  saveOutput(FILE_LOCATION, CONFIG, { ...ENV_VARIABLES, toIgnore: "true" });

  const output = fs.readFileSync(FILE_LOCATION).toString();
  expect(output).toBe("ENV=prod\nAPP=test");
});
